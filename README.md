# Google Chrome Extension Demo


This includes a manifest.json file which every Google Chrome Extension needs. 
Essentially, it just defines the script(s) that make up the extension. The script I wrote to start off is a very simple
content script. The script turns every paragraph tag into having an orange backgorund. Once the extension is downloaded, 
(it can be tested in developer mode) then one can go to any site and see all the p tags turning orange.
